<?php
/**
 * Slider column
 *
 * @package ChefOehlala
 * @since 2016
 */

	use ChefSections\Wrappers\Template;
	use Cuisine\View\Image;
	use Cuisine\Wrappers\Field;
	use chefSlider\Classes\Column;
	use Cuisine\Utilities\Sort;
	
	$media = $column->getField( 'media' );
	$pagination = $column->getField('paginationThumb');
	$media = Sort::byField( $media, 'position', 'ASC' );

if( !empty( $media ) ){
	
	echo '<div class="column slider-column">';
	
		echo '<div class="'.$column->sliderClass().'">';
	
			foreach( $media as $img ){
					
				$full = Image::getMediaUrl( $img['img-id'], 'full' );
				$med = Image::getMediaUrl( $img['img-id'], 'medium' );

				if( !$med || $med == '' )
					$med = $column->getField( 'full' );

				echo '<div class="gallery-cell">';
					echo '<img src="'.$med.'" data-flickity-lazyload="'.$full.'"/>';
				echo '</div>';
			}
	
		echo '</div>';
		
		if ( $pagination == "thumbs" ) {

			echo '<div class="gallery-nav fullwidth gallery-slider">';

				foreach( $media as $img ){
					
					$thumb = Image::getMediaUrl( $img['img-id'], 'thumbnail' );
	
					echo '<img class="gallery-cell" src="'.$thumb.'" data-flickity-lazyload="'.$thumb.	'"/>';
	
				}
			echo '</div>';

		}

	echo '</div>';
	
}
define([
	
	'jquery',
	'flickity',

], function( $, Flickity ){

	$(document).ready(function(){


		var flickity = new Flickity( '.main-gallery', {
  			imagesLoaded: true,
  			wrapAround: Slider.wrap,
  			autoPlay: Slider.speed,
  			pageDots: Slider.hidePagination,
  			cellAlign: Slider.alignment,
  			prevNextButtons: Slider.navButtons
		});


		/**
		 * Add navigation, only if needed:
		 */
		if( $('.gallery-nav').length > 0 ){

			requirejs(['flickity-as-nav-for/as-nav-for'], function( Flickity ){

				var flickityNav = new Flickity( '.gallery-nav', {
				  	asNavFor: '.main-gallery',
				  	contain: true,
				 	pageDots: false,
				 	prevNextButtons: false,
					cellAlign: Slider.alignment
				});
			
			});
		}

	});
});
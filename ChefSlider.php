<?php
/**
 * Plugin Name: Chef Slider Column
 * Plugin URI: http://chefduweb.nl/plugins/chef-slider
 * Description: Adds a slider column to Chef Sections
 * Version: 1.0
 * Author: Pascal van Gaal
 * Author URI: http://www.chefduweb.nl/
 * License: GPLv2
 * 
 * @package Cuisine
 * @category Core
 * @author Chef du Web
 */

namespace ChefSlider;

use Cuisine\Utilities\Url;
use Cuisine\Wrappers\Script;
use Cuisine\Wrappers\Sass;


class ChefSlider{

	/**
	 * Assets instance.
	 *
	 * @var \ChefSlider\ChefSlider
	 */
	private static $instance = null;


	/**
	 * Init admin events & vars
	 */
	function __construct(){

		//register column:
		$this->registerColumn();

		//load the right files
		$this->loadIncludes();

		//assets:
		$this->enqueues();


	}

	/**
	 * Register this column-type with Chef Sections
	 * 
	 * @return void
	 */
	private function registerColumn(){


		add_filter( 'chef_sections_column_types', function( $types ){

			$base = Url::path( 'plugin', 'chef-slider', true );
			$types['slider'] = array(
						'name'		=> 'Slider',
						'class'		=> 'ChefSlider\Column',
						'template'	=> $base.'Assets/template.php'
			);

			return $types;

		});

	}

	/**
	 * Load all includes for this plugin
	 * 
	 * @return void
	 */
	private function loadIncludes(){

		include( 'Classes/Column.php' );

	}


	/**
	 * Enqueue scripts & Styles
	 * 
	 * @return void
	 */
	private function enqueues(){

		add_action( 'init', function(){

			//scripts:
			$url = Url::plugin( 'chef-slider', true ).'Assets/js/';

			//sass:
			$url = 'chef-slider/Assets/sass/';
			Sass::register( 'flickity', $url.'_flickity.scss', false );
		

			

		});


		add_action( 'admin_menu', function(){

			$url = Url::plugin( 'chef-slider', true ).'Assets';
			wp_enqueue_style( 'chef-slider', $url.'/css/admin.css' );
		
		});
	}

	

	/*=============================================================*/
	/**             Getters & Setters                              */
	/*=============================================================*/



	/**
	 * Init the Assets classes
	 *
	 * @return \ChefSlider\ChefSlider
	 */
	public static function getInstance(){

	    if ( is_null( static::$instance ) ){
	        static::$instance = new static();
	    }
	    return static::$instance;
	}


}

add_action( 'chef_sections_loaded', function(){

	\ChefSlider\ChefSlider::getInstance();

});